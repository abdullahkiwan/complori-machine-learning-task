# Machine Learning Challenge

In this challenge I used the dataset (Boston House Prices), becuase it's one of the most popular machine learning datasets, and becuase I'm interested in the real estate domain.

# Steps:
- Data discovery and visualization.
- Data preparation.
- Training machine learning models.
- Model deployment.

# Data discovery and visualization
In this step I looked at the correlation between the features, and the relationship between each feature and the price.

# Data Preparation
Data preparation includes splitting data into train and test sets, and rescale data.

# Training models:
I experimented with 4 different regression models (LinearRegression - Support Vector Regressor - RandomForest - DecisionTree).

Support Vector Regressor, RandomForest and DecisionTree algorithms require some parameters. Therefore I used cross-validation to search for the combination of parameters that give the best results.

# Packages:
- For data preparation and visualization I used Pandas, Matplotlib, and Seaborn.
- For machine learning algorithms I used Scikit-learn package.
- For the rest api I used FastApi.

# Running the scripts:
Before running the scripts, we create a virtual environment and install the packages:
- `virtualenv venv`
- On Linux : `source venv/bin/activate` - On windows : `venv\scripts\activate.bat`
- `pip install -r requirements.txt`
- `jupyter notebook` To run the experiments.ipynb file.
# Scripts
- experiments.ipynb: The experiments with the 4 machine learning algorithms are found in the jupyter notebook (experiments.ipynb)
- ml_utils.py : it contains some helping functions related to machine learning algorithms.
- data_utils.py : it contains some helping functions related to data preparation.

# Dockerized REST API
To build the docker image and use the rest api, run the following:

- `sudo docker image build --tag myimage .`
- `sudo docker container run --publish 80:80 --name mycontainer myimage`

I created two endpoints in the api:
- train: 
This is a necessary step before running predictions.
A name of the output model should be provided so that it is used in the prediction process.

`curl -X POST http://0.0.0.0:80/train -H "Content-Type: application/json" -d '{"model_type":"RandomForest", "with_search_params": "True", "data_file": "BostonHousing.csv", "output_model":"RandomForestModel.model" }'`

- predict: 
It is necessary to provide the name of the model that was trained before.

`curl -X POST http://0.0.0.0:80/predict -H "Content-Type: application/json" -d '{"features":"[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]", "model_path":"RandomForestModel.model" }'`