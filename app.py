
from fastapi import FastAPI
from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse
from pydantic import BaseModel
import pandas as pd

from ml_utils import train_model, evaluate_model, predict_price
from data_utils import prepare_data

class TrainInput(BaseModel):
    data_file: str
    model_type: str
    with_search_params: str
    output_model: str
    
class PredictInput(BaseModel):
    features: list
    model_path: str

app = FastAPI()

@app.get("/hello")
def hello():
    return {"Hello": "Complori"}

@app.post("/train")
def train(inp: TrainInput):

    target_col = 'medv'
    
    data_file = inp.data_file
    model_type = inp.model_type
    with_search_params = eval(inp.with_search_params)
    output_model = inp.output_model

    df = pd.read_csv(data_file)
    X_train, y_train, X_test, y_test = prepare_data(df, target_col)
    
    train_model(model_type, with_search_params, output_model, X_train, y_train, X_test, y_test)
    
    return {"success": "Model was trained successfully."}

@app.post("/predict")
def predict(inp: PredictInput):
    
    features = inp.features
    model_path = inp.model_path
    
    predicted_price = predict_price(features, model_path)
    
    return {"predicted_price": predicted_price}
