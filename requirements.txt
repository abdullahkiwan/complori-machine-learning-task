notebook
scikit-learn
pandas
joblib
matplotlib
seaborn
fastapi
uvicorn[standard]
